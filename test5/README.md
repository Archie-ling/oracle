# 实验5：包，过程，函数的用法

姓名：凌晟淇    学号：202010414110

## 实验目的

- 了解`PL/SQL`语言结构

- 了解`PL/SQL`变量和常量的声明和使用方法

- 学习包，过程，函数的用法。

## 实验内容

- 以hr用户登录

1. 创建一个包(Package)，包名是MyPack。

2. 在MyPack中创建一个函数Get_SalaryAmount,输入的参数是部门ID，通过查询员工表，统计每个部门的salay工资总额。

3. 在MyPack中创建一个过程GET_EMPLOYEES,输入参数是员工ID，在过程中使用游标，通过查询员工表，递归查询某个员工及其所有下属，子下属员工。 Oracle递归查询的语句格式是：

   ~~~sql
   SELECT LEVEL,EMPLOYEE_ID,FIRST_NAME,MANAGER_ID FROM employees 
   START WITH EMPLOYEE_ID = V_EMPLOYEE_ID 
   CONNECT BY PRIOR EMPLOYEE_ID = MANAGER_ID
   ~~~

## 实验步奏

1. **以hr用户登录，创建MyPack包**

   ~~~sql
   create or replace PACKAGE MyPack IS
   FUNCTION Get_SalaryAmount(V_DEPARTMENT_ID NUMBER) RETURN NUMBER;
   PROCEDURE Get_Employees(V_EMPLOYEE_ID NUMBER);
   END MyPack;
   /
   ~~~

   ![](./createpack.png)

2. **创建有关函数和过程**

   ~~~sql
   create or replace PACKAGE BODY MyPack IS
   FUNCTION Get_SalaryAmount(V_DEPARTMENT_ID NUMBER) RETURN NUMBER
   AS
       N NUMBER(20,2); --注意，订单ORDERS.TRADE_RECEIVABLE的类型是NUMBER(8,2),汇总之后，数据要大得多。
       BEGIN
       SELECT SUM(salary) into N  FROM EMPLOYEES E
       WHERE E.DEPARTMENT_ID =V_DEPARTMENT_ID;
       RETURN N;
       END;
   
   PROCEDURE GET_EMPLOYEES(V_EMPLOYEE_ID NUMBER)
   AS
       LEFTSPACE VARCHAR(2000);
       begin
       --通过LEVEL判断递归的级别
       LEFTSPACE:=' ';
       --使用游标
       for v in
           (SELECT LEVEL,EMPLOYEE_ID, FIRST_NAME,MANAGER_ID FROM employees
           START WITH EMPLOYEE_ID = V_EMPLOYEE_ID
           CONNECT BY PRIOR EMPLOYEE_ID = MANAGER_ID)
       LOOP
           DBMS_OUTPUT.PUT_LINE(LPAD(LEFTSPACE,(V.LEVEL-1)*4,' ')||
                               V.EMPLOYEE_ID||' '||v.FIRST_NAME);
       END LOOP;
       END;
   END MyPack;
   /
   ~~~

   ![](./createfunction.jpg)

3. 函数`Get_SalaryAmount()`测试方法：

   ~~~sql
   select department_id,department_name,MyPack.Get_SalaryAmount(department_id) AS salary_total from departments;
   ~~~

   ![](./testgetsalary.jpg)

4. **测试过程Get_Employees()**

   ~~~sql
   set serveroutput on
   DECLARE
   V_EMPLOYEE_ID NUMBER;    
   BEGIN
   V_EMPLOYEE_ID := 101;
   MYPACK.Get_Employees (  V_EMPLOYEE_ID => V_EMPLOYEE_ID) ;    
   END;
   /
   ~~~

   ![](./testprodure.jpg)

   ## 实验总结

   我通过本次实验学习了PL/SQL，这是Oracle数据库的过程式编程语言，它可以把逻辑控制和SQL语句结合起来。我知道PL/SQL代码一般由块构成，每个块有声明、执行和异常处理三部分，这样便于阅读和维护。

   我还学习了包、过程和函数的概念和用法，它们对于组织和管理代码很有帮助。包是一种把相关对象（如过程、函数、类型等）封装在一起的方法；过程和函数是执行特定任务的可重用程序单元，区别在于过程没有返回值，而函数有一个返回值。我通过编写代码实践了如何创建和调用包、过程和函数，以及如何根据不同场景选择使用它们。
